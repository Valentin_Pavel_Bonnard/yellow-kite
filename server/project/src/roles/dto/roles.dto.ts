import { UsersEntity } from '../../users/users.entity';

// TODO: use class-validaator

export class RolesDto {
  id: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  users?: UsersEntity[];
}