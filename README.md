# yellow kite

## Features

- [Angular 8.X](https://angular.io/) (w/ [TypScript](https://www.typescriptlang.org/))
- [GraphQL](https://graphql.org/)
- [NestJS](https://nestjs.com/) server
- [TypeORM](https://typeorm.io/#/)
- [Apollo](https://www.apollographql.com/)
- [Bootstrap 4](https://getbootstrap.com/)
- [Postgres](https://www.postgresql.org/)
- Authentication w/ [JWT](https://jwt.io/) and [local storage](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API)
- Authorization
- User Management
- Entity Management
- E2E testing
- [Docker](https://www.docker.com/) Compose

<a name="tableofcontents"></a>